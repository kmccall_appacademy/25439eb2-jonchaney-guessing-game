# I/O Exercises
#
# * Write a `guessing_game` method. The computer should choose a number between
#   1 and 100. Prompt the user to `guess a number`. Each time through a play loop,
#   get a guess from the user. Print the number guessed and whether it was `too
#   high` or `too low`. Track the number of guesses the player takes. When the
#   player guesses the number, print out what the number was and how many guesses
#   the player needed.
def game
  def guessing_game
    introduction
    rand_num = random_number
    num_guesses = 1
    guess = get_guess
    until won?(guess, rand_num)
      puts "You guessed:"
      puts guess
      puts high_or_low(guess, rand_num)
      puts "Number of guesses:"
      puts num_guesses
      puts "Guess again: "
      guess = get_guess
      num_guesses += 1
    end
    puts "Number of guesses:"
    puts num_guesses
    conclude(guess)
  end

  def high_or_low(guess, rand_num)
    return "Guess is too high!" if guess > rand_num
    return "Guess is too low!" if guess < rand_num
  end

  def won?(guess, rand_num)
    guess == rand_num
  end

  def get_guess
    gets.chomp.to_i
  end

  def random_number
    1 + rand(10)
  end

  def conclude(guess)
    puts "Congratulations, you won! The correct number was: "
    puts guess
  end

  def introduction
    puts "guess a number"
  end
end

# * Write a program that prompts the user for a file name, reads that file,
#   shuffles the lines, and saves it to the file "{input_name}-shuffled.txt". You
#   could create a random number using the Random class, or you could use the
#   `shuffle` method in array.

def main
  file = prompt
  contents = File.readlines(file)
  contents.shuffle!
  f = File.open("#{file}-shuffled.txt", "w")
  contents.each do |line|
    f.puts line
  end
  f.close
end

def prompt
  print "Enter file name you wish to shuffle: "
  gets.chomp
end

main
